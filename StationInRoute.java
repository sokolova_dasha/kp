package model;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by даша on 08.04.14.
 */
@Entity
public class StationInRoute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Route route;
    private Station station;
    private Integer integer;
    @Temporal(TemporalType.TIME)
    private Date time_wait;
    private Date time_departure;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Date getTime_wait() {
        return time_wait;
    }

    public void setTime_wait(Date time_wait) {
        this.time_wait = time_wait;
    }

    public Date getTime_departure() {
        return time_departure;
    }

    public void setTime_departure(Date time_departure) {
        this.time_departure = time_departure;
    }
}
