package web;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.Date;
import model.*;
/**
 * Created with IntelliJ IDEA.
 * User: даша
 * Date: 25.11.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean(name = "searchBean")
@SessionScoped
public class SearchBean {

    // Объекты для формирования заказа, доступные на протяжении всей сессии пользователя
    // Пользователь, оформляющий заказ
    private Passenger passenger;
    // Выбранный рейс
    private Trip choosenTrip;
    // Выбранный тип билета
    private Integer type_ticket;
    // Формируемый заказ
    private Purchase newPurchase;
    // Кол-во билетов
    private Integer count_ticket;
    // Цена билета
    private Integer price_ticket;
    // Сумма заказа
    private Integer sum_order;
    // Предыдущая страница
    private String preceding_page;
    //сообщения на странице
    private String message;
  // мои заказы
    private ArrayList<Purchase> myPurchases;

    public Trip getChoosenTrip() {
        return choosenTrip;
    }

    public void setChoosenTrip(Trip choosenTrip) {
        this.choosenTrip = choosenTrip;
    }

    public Integer getType_ticket() {
        return type_ticket;
    }
    public void setType_ticket(Integer type_ticket) {
        this.type_ticket = type_ticket;
    }
    public Integer getCount_ticket() {
        return count_ticket;
    }
    public void setCount_ticket(Integer count_ticket) {
        this.count_ticket = count_ticket;
    }
    public Integer getSum_order() {
        return sum_order;
    }

    public void setSum_order(Integer sum_order) {
        this.sum_order = sum_order;
    }
    public Integer getPrice_ticket() {
        return price_ticket;
    }
    public void setPrice_ticket(Integer price_ticket) {
        this.price_ticket = price_ticket;
    }
    public String getPreceding_page() {
        return preceding_page;
    }
    public void setPreceding_page(String preceding_page) {
        this.preceding_page = preceding_page;
    }
    public Passenger getPassenger() {
        return passenger;
    }
    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
    public Purchase getNewPurchase() {
        return newPurchase;
    }
    public void setNewPurchase(Purchase newPurchase) {
        this.newPurchase = newPurchase;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public ArrayList<Purchase> getMyPurchases() {
        return myPurchases;
    }

    public void setMyPurchases(ArrayList<Purchase> myPurchases) {
        this.myPurchases = myPurchases;
    }
    //index
    // Первая страница,поиск рейсов
    private String from;
    private String to;
    private Date day;

    private ArrayList<Trip> findTrip;
    private Integer id_trip;

    public Date getDay() {
        return day;
    }
    public void setDay(Date day) {
        this.day = day;
    }
    public String getFrom() {
        return from;
    }
    public void setFrom(String from) {
        this.from = from;
    }
    public String getTo() {
        return to;
    }
    public void setTo(String to) {
        this.to = to;
    }
    public Integer getId_trip() {
        return id_trip;
    }
    public void setId_trip(Integer id_trip) {
        this.id_trip = id_trip;
    }
    public ArrayList<Trip> getFindTrip() {
        return findTrip;
    }

    public void setFindTrip(ArrayList<Trip> findTrip) {
        this.findTrip = findTrip;
    }


/*
    //Поиск
    public String search() {
        //Поиск по городам
        if (day==null){
            setFrom("Новосибирск");
            setTo("Москва");
            findTrip =new Trip();
        }
        //Поиск по городам и датам
        else{
            findTrip =dao.getCheduleByDate(from, to, new java.sql.Date(day.getTime()));
        }
        return "chooseFight";
    }

    // chooseFight
    // Вторая страница,выбор рейса из найденных
    private ArrayList<Tickets> findTickets;
    public ArrayList<Tickets> getFindTickets() {
        return findTickets;
    }
    public void setFindTickets(ArrayList<Tickets> findTickets) {
        this.findTickets = findTickets;
    }
    //Выбор рейса,список доступных билетов
    public String chooseFight(){
      choosenTrip = dao.getCheduleById(id_trip);
      findTickets=dao.getTicketsByIdOfFight(id_trip);
      return "chooseType";
    }

    // chooseCount
    //четвертая страница,указание количества билетов
    //формироание заказа,проверка наличия билетов
    public String formOrder(){
        //если достаточное количество билетов в наличии
        if (dao.testCountOfTickets(id_trip,type_ticket,count_ticket)){
            sum_order=price_ticket*count_ticket;
            return "formOrder";
        }
        // если недостаточное
        else{
        count_ticket=null;
        return "chooseCount";
        }
    }
    // formOrder
    //подтверждение заказа,проверка пользователя
    public String confirmOrder(){
        //если пользователь авторизован в системе
        if (passenger!=null){
          dao.newPurchase(passenger.getId_passenger(),id_trip,type_ticket,count_ticket,sum_order);
          return "orderReady";
        }
        else{
            return "authorization";
        }

    }
    private String login;
    private String password1;
    private String password2;

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword1() {
        return password1;
    }
    public void setPassword1(String password1) {
        this.password1 = password1;
    }
    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
    // authorization
    public String authorization(){
        message=null;
        passenger=dao.authPassenger(login, password1) ;
       if(passenger!=null){
           { return preceding_page;}
       }
       else{
           message="Имя или пароль введены не верно";
           return "authorization";
       }
    }

  //переход на страницу регистрации
    public String pageRegister(){
        return "register";
    }
    //зарегистрироваться
    public String register(){
        message=null;
        if(password1.equals(password2)){
            dao.newPassenger(login, password1);
            return "authorization";
        }
        else{
            message="Пароли не совпадают";
            password1=null;
            password2=null;
            return "register";
        }
    }

    //ссылки на страницы
    public String toIndex(){
        return "index";
    }
    public String toPreceding(){
        return preceding_page;
    }

    //мои заказы
    public String myPurchases(){
        if(passenger!=null)  {
            myPurchases= dao.getMyOrder(passenger.getId_passenger());
            if (myPurchases.isEmpty()){
                return "myPurchases";
            }
            else{
                choosenTrip =null;
                findTrip.clear();
                for (Purchase order:myPurchases) {
                    choosenTrip =dao.getCheduleById(order.getId_trip());
                    findTrip.add(choosenTrip);
                }
                return "myPurchases";
            }
        }
        else{
            return "authorization";
        }

    }

*/
}
