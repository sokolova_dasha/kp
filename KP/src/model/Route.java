package model;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by даша on 08.04.14.
 */
@Entity
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String station_start;
    private String station_finish;
    @Temporal(TemporalType.TIME)
    private Date time_start;
    @Temporal(TemporalType.TIME)
    private Date time_in_way;
    @OneToMany(mappedBy = "route",fetch = FetchType.EAGER)
    private List<Price> prices;
    @OneToMany(mappedBy = "route",fetch = FetchType.EAGER)
    private List<Trip> trips;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStation_start() {
        return station_start;
    }

    public void setStation_start(String station_start) {
        this.station_start = station_start;
    }

    public String getStation_finish() {
        return station_finish;
    }

    public void setStation_finish(String station_finish) {
        this.station_finish = station_finish;
    }

    public Date getTime_start() {
        return time_start;
    }

    public void setTime_start(Date time_start) {
        this.time_start = time_start;
    }

    public Date getTime_in_way() {
        return time_in_way;
    }

    public void setTime_in_way(Date time_in_way) {
        this.time_in_way = time_in_way;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
