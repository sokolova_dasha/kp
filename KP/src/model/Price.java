package model;

import javax.persistence.*;

/**
 * Created by даша on 08.04.14.
 */
@Entity
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Route route;
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassTrip classTrip;
    private Integer all_count;
    private Integer count_in_presence;
    private Integer summa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public ClassTrip getClassTrip() {
        return classTrip;
    }

    public void setClassTrip(ClassTrip classTrip) {
        this.classTrip = classTrip;
    }

    public Integer getSumma() {
        return summa;
    }

    public void setSumma(Integer sum) {
        this.summa = sum;
    }

    public Integer getAll_count() {
        return all_count;
    }

    public void setAll_count(Integer all_count) {
        this.all_count = all_count;
    }

    public Integer getCount_in_presence() {
        return count_in_presence;
    }

    public void setCount_in_presence(Integer count_in_presence) {
        this.count_in_presence = count_in_presence;
    }
}
