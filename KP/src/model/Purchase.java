package model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by даша on 08.04.14.
 */
@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Passenger passenger;
    private String station_start;
    private String station_finish;
    private Date date_start;
    @Temporal(TemporalType.TIME)
    private Date time_start;
    @Temporal(TemporalType.TIME)
    private Date time_in_way;
    private Date date_finish;
    private String name_class;
    private String description;
    private Integer price;
    private Integer ticket_count;
    private Integer sum_for_count;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public String getStation_start() {
        return station_start;
    }

    public void setStation_start(String station_start) {
        this.station_start = station_start;
    }

    public String getStation_finish() {
        return station_finish;
    }

    public void setStation_finish(String station_finish) {
        this.station_finish = station_finish;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getTime_start() {
        return time_start;
    }

    public void setTime_start(Date time_start) {
        this.time_start = time_start;
    }

    public Date getTime_in_way() {
        return time_in_way;
    }

    public void setTime_in_way(Date time_in_way) {
        this.time_in_way = time_in_way;
    }

    public Date getDate_finish() {
        return date_finish;
    }

    public void setDate_finish(Date date_finish) {
        this.date_finish = date_finish;
    }

    public String getName_class() {
        return name_class;
    }

    public void setName_class(String name) {
        this.name_class = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTicket_count() {
        return ticket_count;
    }

    public void setTicket_count(Integer ticket_count) {
        this.ticket_count = ticket_count;
    }

    public Integer getSum_for_count() {
        return sum_for_count;
    }

    public void setSum_for_count(Integer sum) {
        this.sum_for_count = sum;
    }
}
