package dao;

import model.*;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;

/**
 * Created by даша on 28.04.14.
 */
@Local
public interface IDAOLocal {
    //найти рейсы по стнанциям и/или дате отправления
    public List<Trip> getRoutesAndTripBySityAndDate(String from,String to,Date day_start);
    //обновление цены(измененено количество билетов)
    public void updatePrice(Price price);
    // сохранить покупку
    public void savePurchase(Purchase purchase);
    // Мои покупки
    public List<Purchase> getMyPurchase(Integer id_passenger);
    // Зарегистрировать пользователя
    public void  addUser(Passenger passenger);
    // Авторизация пользователя
    public boolean authUser(Passenger passenger);
}
